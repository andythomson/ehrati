package view;

import controller.*;
import model.Model;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by andrew-thomson on 18/11/2017.
 */
public class GUI extends JFrame implements Observer {

    Model guiModel;
    private JTabbedPane panel;
    private JSplitPane historyPanel;
    private JSplitPane medicationPanel;
    private JSplitPane recordPanel;
    private JSplitPane allHistoryPanel;
    private KeyPressListener listener;
    //private SchedulerListener sl;
    //private MouseClickListener ml;
    private MenuActionListener mal;
    ArrayList<ActionJToggleButton> recordButtons;
    ArrayList<JToggleButton> historyButtons;
    Data dat;
    Container cont;

    SchedulerListener sl;
    MouseClickListener mcl;

    public GUI(Model inputModel) {
        guiModel = inputModel;
        sl = new SchedulerListener(guiModel, this);
        sl.startTimer();
        mcl = new MouseClickListener(guiModel, sl, this);
        MouseScrollListener msl = new MouseScrollListener(guiModel, this);


        this.setSize(1550, 1000);
        this.setTitle("Electronic Health Record");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        recordButtons = new ArrayList<>();
        historyButtons = new ArrayList<>();

        mal = new MenuActionListener(this, guiModel, sl);
        cont = this.getContentPane();
        panel = new JTabbedPane();
        getContentPane().add(panel);

        JPanel topPanel = new JPanel();
//        topPanel.setPreferredSize(new Dimension(200, this.getHeight()));
        JSplitPane splitPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPanel, panel);

        cont.add(splitPanel);

        makeButtons(topPanel, mal);

        dat = new Data(guiModel);

        //For each physician
        historyPanel = dat.historyMode();
        panel.add(historyPanel, "HISTORY");
        //For each entry into the system
        recordPanel = dat.recordMode();
        panel.add(recordPanel, "RECORD");
        //View Medication
        medicationPanel = dat.medicationMode();
        panel.add(medicationPanel, "MEDICATION");
        //View All History
        allHistoryPanel = dat.allHistoryMode();
        panel.add(allHistoryPanel, "NOTES");

        this.add(panel, BorderLayout.BEFORE_LINE_BEGINS);

        makeMenuBar(this, mal);

        System.out.println("Hello this is to see if repaint is running ");
        panel.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        panel.addTab("History", historyPanel);
        panel.addTab("Record", recordPanel);
        panel.addTab("Medication", medicationPanel);
        panel.addTab("Notes", allHistoryPanel);
        listener = new KeyPressListener(guiModel, sl, this);

        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    public void switchMode() {
        CardLayout cl = (CardLayout) panel.getLayout();
        if (guiModel.getState().equals("RECORD")) {

            cl.show(panel, "RECORD");


        } else if (guiModel.getState().equals("HISTORY")) {
            System.out.println("Show panel history");
            cl.show(panel, "HISTORY");

        } else if (guiModel.getState().equals("MED")) {
            System.out.println("Show panel Medication");
            cl.show(panel, "MEDICATION");

        }
    }

    private void makeMenuBar(GUI frame, MenuActionListener mal) {
        JMenuBar menubar = new JMenuBar();
        frame.setJMenuBar(menubar);

        JMenu fileMenu = new JMenu("File");
        menubar.add(fileMenu);

        JMenuItem newRecord = new JMenuItem("New");
        newRecord.addActionListener(mal);
        fileMenu.add(newRecord);

        JMenuItem openRecord = new JMenuItem("Open");
        openRecord.addActionListener(mal);
        fileMenu.add(openRecord);

        JMenuItem saveRecord = new JMenuItem("Save");
        saveRecord.addActionListener(mal);
        fileMenu.add(saveRecord);

        if (guiModel.getState().equals("RECORD")) {
            JMenuItem clearRecord = new JMenuItem("Clear");
            clearRecord.addActionListener(mal);
            fileMenu.add(clearRecord);
        }
        if (guiModel.getState().equals("RECORD")) {
            JMenuItem refreshRecord = new JMenuItem("Refresh");
            refreshRecord.addActionListener(mal);
            fileMenu.add(refreshRecord);
        }
    }

    private void makeButtons(JPanel button, MenuActionListener mal) {
        JLabel pat = new JLabel("<html>Patient</html>", SwingConstants.CENTER);
        button.add(pat);
        JButton patientDownListener = new JButton("<");
        patientDownListener.addActionListener(mal);
        patientDownListener.setToolTipText("Click to change patient down");
        patientDownListener.setActionCommand("PatientDown");
        button.add(patientDownListener);
        JButton patientUpListener = new JButton(">");
        patientUpListener.addActionListener(mal);
        patientUpListener.setToolTipText("Click to change patient up");
        patientUpListener.setActionCommand("PatientUp");
        button.add(patientUpListener);

        JLabel adm = new JLabel("<html>Admission</html>", SwingConstants.CENTER);
        button.add(adm);
        JButton admissionDownListener = new JButton("<");
        admissionDownListener.addActionListener(mal);
        admissionDownListener.setToolTipText("Click to change admission down");
        admissionDownListener.setActionCommand("AdmissionDown");
        button.add(admissionDownListener);
        JButton admissionUpListener = new JButton(">");
        admissionUpListener.addActionListener(mal);
        admissionUpListener.setToolTipText("Click to change admission up");
        admissionUpListener.setActionCommand("AdmissionUp");
        button.add(admissionUpListener);


    }
//

    public String getSelectedButton() {
        for (ActionJToggleButton btn : recordButtons) {
            if (btn.isSelected()) {
                return btn.getCommand();
            }
        }
        return "";
    }

    public String getSelectedHistoryButton() {
        String selectedButton = "";
        for (JToggleButton btn : historyButtons) {

            if (btn.isSelected()) {
                selectedButton = btn.getText();
                btn.setSelected(false);
            }
        }
        return selectedButton;
    }


    @Override
    public void update(Observable o, Object arg) {

//        Data d = new Data(guiModel);
//        historyPanel = d.historyMode();
//        recordPanel = d.recordMode();
//        medicationPanel = d.medicationMode();
//        allHistoryPanel = d.allHistoryMode();
        repaint();

    }
}
