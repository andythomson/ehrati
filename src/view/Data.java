package view;

import model.Drugs;
import model.Model;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by andrew-thomson on 28/01/2018.
 */
public class Data extends JFrame {
    DefaultTableModel tabMod;
    Model model;

    public Data(Model mod) {
        model = mod;
//        clearTable();
    }

    public JSplitPane historyMode() {
        JPanel historyPanel = new JPanel();
        historyPanel.setBackground(Color.DARK_GRAY);
        CardLayout historyLayout = new CardLayout();

        JPanel info = new JPanel();
        info.setLayout(historyLayout);

        int hadm = model.getHadm();

        //Add info part  patient info
        JLabel labelAdmission = new JLabel("<html><h1 style=\"color:#70a35b;padding:10px;\">Current Admission</h1><p style=\"padding:10px;\">" +
                "<b>Admission ID: </b>" + hadm +
                " <br/><br/><b>Admission Type:</b> " + model.getAdmisType() +
                "<br/><b>Subject ID:</b> " + model.getSub() +
                " <br/><b>Location:</b> " + model.getAdmisLoc() +
                " <br/><b>Diagnosis:</b> " + model.getDiagnosis() +
                " <br/><br/><b>Care Giver:</b> " + model.getCGID() +
                " <br/><b>Care Giver Category:</b> " + model.getCategory() +
                "</p><br/><br/></html>");
        labelAdmission.setFont(new Font("Myriad Pro", Font.PLAIN, 15));
        info.add(labelAdmission);
        historyPanel.add(info);

        System.out.println("History mode id "+model.getSub());

        // History from current admission
        JPanel notes = new JPanel();
        notes.setLayout(historyLayout);
        JList<String> arrnotes = new JList(model.getNotes().toArray());


        arrnotes.setFont(new Font("Myriad Pro", Font.PLAIN, 12));

        JScrollPane scroller = new JScrollPane(arrnotes, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        notes.add(scroller);
        notes.setPreferredSize(new Dimension(1000, 800));


        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split.setTopComponent(info);
        split.setBottomComponent(notes);
        split.setDividerLocation(0.8);

        return split;
    }

    public JSplitPane recordMode() {
        JTextArea history;
        JTextArea notes;
        JLabel labelPatient;
        JLabel labelHistory;
        JLabel labelNotes;
        JPanel recordPanel = new JPanel();

        recordPanel.setBackground(Color.DARK_GRAY);
        // info panel
        GridLayout infoLayout = new GridLayout(0, 2);

        JPanel info = new JPanel();
        info.setLayout(infoLayout);

        // input panel
        JPanel input = new JPanel();
        input.setLayout(new GridLayout(5, 1));
        int hadm = model.getHadm();
        //Add info part General info and patient info
        String time = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime());
        int sub = model.getSub();
        long age = model.getAge();
        String drug = model.getDrug();
        String gender = model.getGender();

        labelPatient = new JLabel("<html><h1 style=\"color:#70a35b;padding:10px;\">Patient</h1><p style=\"padding:10px;\">" +
                "<b>Admission ID: </b>" + hadm + " <br/><b>Visit date:</b> " + time +
                "<br/><b>Subject ID:</b> " + sub + "<br/><b>Gender: </b>" + gender + "<br/><b>AGE: </b>" + age + "<br/><b>Allergies:" +
                "</b> Patient recorded as having No Known Allergies to Drugs<br/><b>Current Prescriptions:</b> Albuterol 0.083% Neb Soln <br/><b></b> " + " <br/></p></html>");

        labelPatient.setFont(new Font("Myriad Pro", Font.PLAIN, 15));

        info.add(labelPatient);
        //Add reason and notes text boxes
        history = new JTextArea();
        history.setText("");
        history.setEditable(true);
        history.setFont(history.getFont().deriveFont(14f));
        history.setLineWrap(true);
        history.setBorder(BorderFactory.createCompoundBorder(history.getBorder(), BorderFactory.createEmptyBorder(10, 10, 5, 5)));
        history.setPreferredSize(new Dimension(400, 150));
        JList<String> labelHistoryNotes = new JList(model.getDiagnosesHistory().toArray());
        JScrollPane reasonScroll = new JScrollPane(labelHistoryNotes, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);


        notes = new JTextArea();
        notes.setText("");
        notes.setEditable(true);
        notes.setFont(notes.getFont().deriveFont(14f));
        notes.setLineWrap(true);
        notes.setBorder(BorderFactory.createCompoundBorder(notes.getBorder(), BorderFactory.createEmptyBorder(10, 10, 5, 5)));
        notes.setPreferredSize(new Dimension(400, 150));
        JScrollPane notesScroll = new JScrollPane(notes, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);


        labelHistory = new JLabel("<html><h3 style=\"padding:70px 0px 0px 10px;\">Previous Diagnosis:</h3></html>");
        labelHistory.setFont(new Font("Myriad Pro", Font.PLAIN, 15));
        labelHistory.setPreferredSize(new Dimension(100, 5));
        labelNotes = new JLabel("<html><h3 style=\"padding:70px 0px 0px 10px;\">Notes:</h3></html>");
        labelNotes.setFont(new Font("Myriad Pro", Font.PLAIN, 15));
        labelNotes.setPreferredSize(new Dimension(100, 5));

        input.add(labelHistory);
        input.add(reasonScroll);
        input.add(labelNotes);
        input.add(notesScroll);

        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split.setTopComponent(info);
        split.setBottomComponent(input);
        split.setDividerLocation(0.8);

        return split;
    }

    public JSplitPane medicationMode() {
        JPanel prescriptionPanel = new JPanel();
        prescriptionPanel.setBackground(Color.DARK_GRAY);
        CardLayout prescriptionLayout = new CardLayout();

        JPanel info = new JPanel();
        info.setLayout(prescriptionLayout);

        int hadm = model.getHadm();
        //Add info part General info and patient info


        JLabel labelAdmission = new JLabel("<html><h1 style=\"color:#70a35b;padding:10px;\">Medication</h1><p style=\"padding:10px;\">" +
                "<b>Admission ID: </b>" + hadm +
                "<br/><b>Subject ID:</b> " + model.getSub() +
                " <br/><b>Location:</b> " + model.getAdmisLoc() +
                " <br/><b>Diagnosis:</b> " + model.getDiagnosis() +
                "</p><br/><br/></html>");

        labelAdmission.setFont(new Font("Myriad Pro", Font.PLAIN, 15));

        info.add(labelAdmission);

        prescriptionPanel.add(info, BorderLayout.NORTH);

        JPanel input = new JPanel();
        input.setLayout(new CardLayout());
        //need to load right stuff into each column

        createDrugTable(input);

        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split.setTopComponent(info);
        split.setBottomComponent(input);
        split.setDividerLocation(0.8);


        return split;
    }

    public JTable createDrugTable(JPanel tabPanel) {

        JTable table;
        java.util.List<JTable> tables;
        tables = new ArrayList<JTable>();
        String columnNames[] = {"Drug", "Drug Type", "Dosage", "Method"};

        table = new JTable();
        tabMod = new DefaultTableModel(0, 0);

        tabMod.setColumnIdentifiers(columnNames);
        table.setPreferredScrollableViewportSize(new Dimension(800, 400));
        table.setFillsViewportHeight(true);
        table.setDragEnabled(false);
        table.setEnabled(false);
        table.setModel(tabMod);
        tables.add(table);

        JScrollPane scrollpane = new JScrollPane(table);
        tabPanel.add(scrollpane);
        ArrayList<String> str = new ArrayList<String>();


        for (Iterator<Drugs> it = model.getPrescriptions().iterator(); it.hasNext(); ) {
            Drugs td = it.next();
            String d = td.getDrug();
            String dt = td.getDrugType();
            String s = td.getStrength();
            String r = td.getRoute();
            tabMod.addRow(new Object[]{d, dt, s, r});
        }
        table.setModel(tabMod);
        return table;
    }


    public JSplitPane allHistoryMode() {
        JPanel allHistoryPanel = new JPanel();
        allHistoryPanel.setBackground(Color.DARK_GRAY);
        CardLayout prescriptionLayout = new CardLayout();

        JPanel info = new JPanel();
        info.setLayout(prescriptionLayout);


        //Add info part General info and patient info
        JLabel labelAdmission = new JLabel("<html><h1 style=\"color:#70a35b;padding:10px;\">All Notes</h1><p style=\"padding:10px;\">" +
                "<br/><b>Subject ID:</b> " + model.getSub() +
                "</p><br/><br/></html>");

        labelAdmission.setFont(new Font("Myriad Pro", Font.PLAIN, 15));

        info.add(labelAdmission);

        allHistoryPanel.add(info, BorderLayout.NORTH);
        JPanel input = new JPanel();
        input.setLayout(new CardLayout());
        JList<String> labelHistoryNotes = new JList(model.getNotes().toArray());
        labelHistoryNotes.setFont(new Font("Myriad Pro", Font.PLAIN, 15));
        JScrollPane scroller = new JScrollPane(labelHistoryNotes, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        input.add(scroller);
        input.setPreferredSize(new Dimension(1000, 800));

        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split.setTopComponent(info);
        split.setBottomComponent(input);
        split.setDividerLocation(0.8);


        return split;
    }


    public void clearTable() {
        tabMod.setRowCount(0);
    }
}
