package view;

import javax.swing.*;

/**
 * Created by andrew-thomson on 18/11/2017.
 */
    public class ActionJToggleButton extends JToggleButton {

        private String command;

        public ActionJToggleButton(String text) {
            super(text);
            command = text;
        }

        public String getCommand() {
            return this.command;
        }
}
