package model;

/**
 * Created by andrew-thomson on 15/01/2018.
 */
public class Cleaner {

    public Cleaner() {


    }


    public String removeReturns(String s) {
//        s = s.replaceAll("\\n", "");
//        s = s.replaceAll("\\r", "");
//        s = s.replaceAll("Patient", "<html><br/><html>Patient");
        s = s.replaceAll("([.])", ".<html><br/><html>");


        return s;
    }

    public String sortSpelling(String lines) {
        lines = lines.replaceAll("Pt", "Patient");
        lines = lines.replaceAll("pt", "patient");
        lines = lines.replaceAll("  ", " ");
        lines = lines.replaceAll("   ", " ");

        return lines;
    }


    public static String properCase(String inputVal) {
        // Empty strings should be returned as-is.
        if (inputVal.length() == 0) return "";
        // Strings with only one character uppercased.
        if (inputVal.length() == 1) return inputVal.toUpperCase();
        // Otherwise uppercase first letter, lowercase the rest.
        return inputVal.substring(0, 1).toUpperCase()
                + inputVal.substring(1).toLowerCase();
    }
}
