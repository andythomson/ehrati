package model;

import java.sql.*;

/**
 * Created by andrew-thomson on 10/01/2018.
 */
public class Patient {
    private Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    public int sub;
    public String gender;

    public Patient(int subject_id) {
        con = DBConnect.getConnection();
        sub = subject_id;
        try {
            st = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs = st.executeQuery("select * from mimiciii.patients where subject_id = " + sub);
            ResultSetMetaData rsmd = rs.getMetaData();
            while (rs.next()) {
                sub = rs.getInt("subject_id");

                gender = rs.getString("gender");

//                System.out.println();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getSub() {

        return sub;
    }

    public String getGender() {
        return gender;
    }


}
