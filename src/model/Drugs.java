package model;

/**
 * Created by andrew-thomson on 23/01/2018.
 */
public class Drugs {
    public String drug;
    public String drugtype;
    public String poe;
    public String generic;
    public String formulary;
    public String gsn;
    public String ndc;
    public String strength;
    public String dose;
    public String doserx;
    public String val;
    public String unit;
    public String route;


    public Drugs(String dr, String drty, String form, String str, String ro, String dos) {
        drug = dr;
        drugtype = drty;
        formulary = form;
        strength = str;
        route = ro;
        dose = dos;

    }


    public String getDrugType() {
        return drugtype;
    }


    public String getDrug() {
        return drug;
    }

    public String getFormulary() {
        return formulary;
    }

    public String getStrength() {
        return strength;
    }

    public String getRoute() {
        return route;
    }

    public String getDose() {
        return dose;
    }
}
