package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by andrew-thomson on 10/01/2018.
 */
public class Prescriptions {
    private Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    int sub;
    public String drug;
    public String drugtype;
    public String poe;
    public String generic;
    public String formulary;
    public String gsn;
    public String ndc;
    public String strength;
    public String dose;
    public String doserx;
    public String val;
    public String unit;
    public String route;
    public ArrayList<Drugs> allPrescriptions;

    public Drugs dru;


    public Prescriptions(int subject_id, int hadmid) {
        allPrescriptions = new ArrayList<Drugs>();
        con = DBConnect.getConnection();
        sub = subject_id;

        try {
            st = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs = st.executeQuery("select * from mimiciii.prescriptions  where subject_id = " + sub + "and hadm_id = " + hadmid);
            ResultSetMetaData rsmd = rs.getMetaData();
            while (rs.next()) {


                String hadm = rs.getString("hadm_id");
                drugtype = rs.getString("drug_type");
                drug = rs.getString("drug");
                poe = rs.getString("drug_name_poe");
                generic = rs.getString("drug_name_generic");
                formulary = rs.getString("formulary_drug_cd");
                gsn = rs.getString("gsn");
                ndc = rs.getString("ndc");
                strength = rs.getString("prod_strength");
                dose = rs.getString("dose_val_rx");
                doserx = rs.getString("dose_unit_rx");
                val = rs.getString("form_val_disp");
                unit = rs.getString("form_unit_disp");
                route = rs.getString("route");
                dru = new Drugs(drug, drugtype, formulary, strength, route, dose);
                allPrescriptions.add(dru);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getDrug() {
        return drug;
    }

    public ArrayList<String> getAllPrescriptions() {
        ArrayList<String> str = new ArrayList<String>();
        for (Iterator<Drugs> it = allPrescriptions.iterator(); it.hasNext(); ) {
            Drugs td = it.next();
//            for(Drugs td:allPrescriptions){
            str.add(td.getDrug() + "  " + td.getDrugType() + " " + td.getStrength() + " " + td.getRoute());
        }
        return str;
    }

    public ArrayList<Drugs> getPrescriptions() {
        return allPrescriptions;
    }

    public String getDrugType() {
        return drugtype;
    }

    public String getPoe() {
        return poe;
    }

    public String getGeneric() {
        return generic;
    }

    public String getFormulary() {
        return formulary;
    }

    public String getGsn() {
        return gsn;
    }

    public String getNdc() {
        return ndc;
    }

    public String getStrength() {
        return strength;
    }

    public String getDose() {
        return dose;
    }

    public String getDoserx() {
        return doserx;
    }

    public String getVal() {
        return val;
    }

    public String getUnit() {
        return unit;
    }

    public String getRoute() {
        return route;
    }
}
