package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by andrew-thomson on 11/01/2018.
 */
public class NoteAdmission {
    private Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    int sub;
    public String notes;
    public String category;
    public String description;
    public String label;
    public String des;
    public int careID;
    public int adm;
    public ArrayList<String> allNotes;

    public NoteAdmission(int subject_id, int hadm) {
        allNotes = new ArrayList<String>();
        con = DBConnect.getConnection();
        sub = subject_id;
        try {
            st = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs = st.executeQuery("select *,c.description as caredescription, note.description as notedescription, note.category as notecategory from mimiciii.noteevents note inner join mimiciii.caregivers c on note.cgid = c.cgid where note.subject_id = " + sub + "and note.hadm_id = " + hadm);
            ResultSetMetaData rsmd = rs.getMetaData();
            while (rs.next()) {

                careID = rs.getInt("cgid");
                label = rs.getString("label");
                des = rs.getString("caredescription");
                adm = rs.getInt("hadm_id");
                notes = rs.getString("text");
                allNotes.add(notes);

                category = rs.getString("notecategory");
                description = rs.getString("notedescription");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getCGID() {
        return careID;
    }

    public String getLabel() {
        return label;
    }

    public String getCareDesc() {
        return des;
    }

    public ArrayList<String> getNotes() {
        ArrayList<String> cleanedNotes = new ArrayList();
        for (Iterator<String> it = allNotes.iterator(); it.hasNext(); ) {
            String td = it.next();
            Cleaner c = new Cleaner();
            String n = c.sortSpelling(td);
//            n = c.removeReturns(n);
            cleanedNotes.add(n);
        }
        return cleanedNotes;
    }

    public String getCategory() {
        return category;
    }

    public String getNotesDesc() {
        return description;
    }


}
