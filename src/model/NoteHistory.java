package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by andrew-thomson on 25/01/2018.
 */
public class NoteHistory {

    private Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    int sub;
    public String notes;
    public String category;
    public String description;
    public String label;
    public String des;
    public int careID;
    public int adm;
    public ArrayList<Note> allNotes;
    public Note n;
    public ArrayList<String> cleanedNotes;

    public NoteHistory(int subject_id) {
        allNotes = new ArrayList<>();
        cleanedNotes = new ArrayList<>();
        con = DBConnect.getConnection();
        sub = subject_id;
        try {
            st = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs = st.executeQuery("select *,c.description as caredescription, note.description as notedescription, note.category as notecategory from mimiciii.noteevents note inner join mimiciii.caregivers c on note.cgid = c.cgid where note.subject_id = " + sub);
            ResultSetMetaData rsmd = rs.getMetaData();
            while (rs.next()) {
                careID = rs.getInt("cgid");
                label = rs.getString("label");
                des = rs.getString("caredescription");
                adm = rs.getInt("hadm_id");
                notes = rs.getString("text");
                category = rs.getString("notecategory");
                description = rs.getString("notedescription");

                n = new Note(category, notes, adm);
                allNotes.add(n);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getCGID() {
        return careID;
    }

    public String getLabel() {
        return label;
    }

    public String getCareDesc() {
        return des;
    }

    public ArrayList<String> getNotes() {

        for (Iterator<Note> it = allNotes.iterator(); it.hasNext(); ) {
            Note td = it.next();

            cleanedNotes.add(td.getAdmission() + " " + td.getCareGiver() + "  " + td.getNotes());

        }
        return cleanedNotes;
    }

    public void deleteNotes() {
        cleanedNotes.removeAll(cleanedNotes);

    }


    public String getCategory() {
        return category;
    }

    public String getNotesDesc() {
        return description;
    }


}
