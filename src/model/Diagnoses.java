package model;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by andrew-thomson on 10/01/2018.
 */
public class Diagnoses {
    private Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    int sub;
    public ArrayList<String> allDiagnosis;

    public String diagnosis;

    public Diagnoses(int subject_id, int hadm) {
        allDiagnosis = new ArrayList<String>();
        con = DBConnect.getConnection();
        sub = subject_id;
        try {
            st = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs = st.executeQuery("select * from mimiciii.diagnoses_icd inner join mimiciii.d_icd_diagnoses on diagnoses_icd.icd9_code = d_icd_diagnoses.icd9_code  where diagnoses_icd.subject_id = " + sub + "and hadm_id = " + hadm);
            ResultSetMetaData rsmd = rs.getMetaData();
            while (rs.next()) {
                diagnosis = rs.getString("long_title");
                allDiagnosis.add(diagnosis);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getDiagnosesHistory() {
        return allDiagnosis;
    }

}
