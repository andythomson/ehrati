package model;

import java.sql.*;
import java.time.temporal.ChronoUnit;

/**
 * Created by andrew-thomson on 10/01/2018.
 */
public class Admissions {
    private Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    int sub;
    public long age;
    public int hadm;
    public Date dob;
    public Date admis;
    public String admtype;
    public String admloc;
    public String disloc;
    public String diag;


    public Admissions(int subject_id, int hadmid) throws SQLException {
        con = DBConnect.getConnection();
        sub = subject_id;
        try {
            st = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs = st.executeQuery("select * from mimiciii.patients inner join mimiciii.admissions on patients.subject_id = admissions.subject_id  where patients.subject_id = " + sub + "and hadm_id = " + hadmid);
            ResultSetMetaData rsmd = rs.getMetaData();
            while (rs.next()) {
                hadm = rs.getInt("hadm_id");
                dob = rs.getDate("dob");
                admis = rs.getDate("admittime");
//                System.out.println(admis);
                admtype = rs.getString("admission_type");
//                System.out.println(admtype);
                admloc = rs.getString("admission_location");
//                System.out.println(admloc);
                disloc = rs.getString("discharge_location");
//                System.out.println(disloc);
                diag = rs.getString("diagnosis");
//                System.out.println(diag);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public long getAge() {
        age = ChronoUnit.YEARS.between(dob.toLocalDate(), admis.toLocalDate());
//        System.out.println("Age at time of appointment: " + age);
        return age;
    }

    public int getHadm() {
        return hadm;
    }

    public String getAdmisType() {

        String adm = Cleaner.properCase(admtype);
        return adm;
    }

    public String getAdmisLoc() {
        String loc = Cleaner.properCase(admloc);
        return loc;
    }

    public String getDisLoc() {
        return disloc;
    }

    public String getDiagnosis() {
        String diago = Cleaner.properCase(diag);

        return diago;
    }
}
