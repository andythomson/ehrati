package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by andrew-thomson on 18/11/2017.
 */
public class Model extends Observable {

    private String currState;
    Integer sub;
    Integer hadm = 132861;
    Patient pa;
    Admissions a;
    Diagnoses d;
    Prescriptions p;
    NoteAdmission n;
    NoteHistory h;


    public Model() throws SQLException {
        currState = "RECORD";
        init();
    }

    //    88025  180431
//    int sub = 87887;
//    int hadm = 132861;
    public void init() throws SQLException {
        System.out.println("New model ID: " + getID());
        if (getID() == null) {
            sub = 87887;
        }

        pa = new Patient(getID());
        a = new Admissions(getID(), getAdmission());
        d = new Diagnoses(getID(), getAdmission());
        p = new Prescriptions(getID(), getAdmission());
        n = new NoteAdmission(getID(), getAdmission());
        h = new NoteHistory(getID());

    }
    public void setID(int id) {

        sub = id;
        this.setChanged();
        this.notifyObservers(this);
    }

    public Integer getID() {
        return sub;
    }


    public Integer getAdmission() {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        con = DBConnect.getConnection();
        try {
            st = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs = st.executeQuery("SELECT max(hadm_id) as adm FROM mimiciii.patients inner join mimiciii.noteevents On patients.subject_id = noteevents.subject_id WHERE noteevents.subject_id= " + sub);
            while (rs.next()) {
                hadm = rs.getInt("adm");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("HADM: " + hadm);


        return hadm;

    }

    public void changePatientUp() {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        con = DBConnect.getConnection();
        try {
            st = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs = st.executeQuery("SELECT Min(subject_id) as nextID FROM mimiciii.patients WHERE subject_id> " + sub);
            while (rs.next()) {
                sub = rs.getInt("nextID");
                System.out.println(sub);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Inside model patient up method" + sub);
        setID(sub);
//        NoteHistory.deleteNotes();
        try {
            init();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        GUI g = new GUI(this);
    }

    public void changePatientDown() {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        con = DBConnect.getConnection();

        try {
            st = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs = st.executeQuery("SELECT Max(subject_id) as nextID FROM mimiciii.patients WHERE subject_id< " + sub);
            ResultSetMetaData rsmd = rs.getMetaData();
            while (rs.next()) {
                sub = rs.getInt("nextID");
                System.out.println(sub);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        setID(sub);
        try {
            init();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        GUI g = new GUI(this);
    }



    public Integer getHadm() {
        return a.getHadm();
    }

    public Integer getSub() {
        return pa.getSub();
    }

    public String getAdmisLoc() {
        return a.getAdmisLoc();
    }

    public String getAdmisType() {
        return a.getAdmisType();
    }

    public String getDiagnosis() {
        return a.getDiagnosis();
    }

    public int getCGID() {
        return n.getCGID();
    }

    public String getCategory() {
        return n.getCategory();
    }

    public ArrayList<String> getNotes() {
        return n.getNotes();
    }

    public long getAge() {
        return a.getAge();
    }

    public String getDrug() {
        return p.getDrug();
    }

    public String getGender() {
        return pa.getGender();
    }

    public ArrayList<String> getDiagnosesHistory() {
        return d.getDiagnosesHistory();
    }

    public ArrayList<Drugs> getPrescriptions() {
        return p.getPrescriptions();
    }


    public void changeAdmission() {

    }

    public String getState() {
        return currState;
    }

    public void setState(String igs) {
        currState = igs;
    }
}
