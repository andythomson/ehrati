package model;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by andrew-thomson on 10/01/2018.
 */
public class DBConnect {
    private static Connection con = null;
    private static String url = "jdbc:postgresql://localhost:5432/mimic";
    private static String user = "andy";
    private static String password = "";

    public static Connection getConnection() {
        try {
            Driver load = (Driver) Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            try {
                con = DriverManager.getConnection(url, user, password);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return con;
    }

}


