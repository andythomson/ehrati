package model;

/**
 * Created by andrew-thomson on 25/01/2018.
 */
public class Note {
    public String careGiver;
    public String notes;
    public int admission;

    public Note(String care, String not, int adm) {
        careGiver = care;
        notes = not;
        admission = adm;
    }

    public String getCareGiver() {
        return careGiver;
    }

    public String getNotes() {
        Cleaner c = new Cleaner();
        String n = c.sortSpelling(notes);
        return n;
    }

    public int getAdmission() {

        return admission;
    }

}
