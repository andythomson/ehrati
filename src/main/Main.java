package main;

import model.Model;
import view.GUI;

import java.awt.*;
import java.sql.SQLException;

/**
 * Created by andrew-thomson on 18/11/2017.
 */
public class Main  {
    public static void main(String[] args) throws Exception{
        EventQueue.invokeAndWait(new Runnable() {
            @Override
            public void run() {
                load();
            }
        });
    }
    public static void load() {
        Model theModel = null;
        try {
            theModel = new Model();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        GUI theGui = new GUI(theModel);
        theModel.addObserver(theGui);
    }




}
