package controller;

import model.Model;
import view.GUI;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 * Created by andrew-thomson on 18/11/2017.
 */
public class MouseScrollListener implements MouseWheelListener {
    private Model model;
    private GUI gui;

    public MouseScrollListener(Model inputModel, GUI gui){
        this.model = inputModel;
        this.gui = gui;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
    }
}
