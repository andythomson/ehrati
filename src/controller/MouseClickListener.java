package controller;

import model.Model;
import view.GUI;

import java.awt.event.MouseEvent;
/**
 * Created by andrew-thomson on 18/11/2017.
 */
public class MouseClickListener implements java.awt.event.MouseListener {
	private Model model;
    private GUI gui;
    private SchedulerListener sl;

	public MouseClickListener(Model inputModel, SchedulerListener inputSl, GUI gui){
		
		this.model = inputModel;
		this.sl = inputSl;
        this.gui = gui;

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		String selection;
         if (model.getState().equals("RECORD")) {
        	selection = gui.getSelectedButton();
        	System.out.println("Record selection: " + selection);
        }
        else {
        	selection = gui.getSelectedHistoryButton();
        	System.out.println("History selection: " + selection);
        }
		switch(selection){
		case "History":
			model.setState("HISTORY");
			System.out.println("(HISTORY) state is: "+ model.getState());
			break;
		case "Record Mode":
			model.setState("RECORD");
			System.out.println("(RECORD) state is: "+ model.getState());
			break;
        }
	}



    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {


    }

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


}