package controller;

import model.Model;
import view.GUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by andrew-thomson on 18/11/2017.
 */
public class MenuActionListener implements ActionListener  {

    private Model model;
    private GUI gui;
    private File file;
    private SchedulerListener sl;

    public MenuActionListener(GUI gui, Model inputModel, SchedulerListener inputSl) {
        this.model = inputModel;
        this.gui = gui;
        sl = inputSl;
        file = null;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        System.out.println("Source: " + ae.getActionCommand());

        if (ae.getActionCommand().equals("Open")) {
            JFileChooser fc = new JFileChooser();
            int fcReturnVal = fc.showOpenDialog(gui);

            if (fcReturnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                setFile(file);
                FileLoader fl = new FileLoader(file, model);
            } else {
                System.out.println("Cancelled");
            }
        } else if (ae.getActionCommand().equals("Save")) {
            JFileChooser fc = new JFileChooser();
            int fcReturnVal = fc.showSaveDialog(gui);

            if (fcReturnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                FileSaver fl = new FileSaver(file, model);
            } else {
                System.out.println("Cancelled");
            }
        } else if (ae.getActionCommand().equals("Clear")) {
        } else if (ae.getActionCommand().equals("Refresh")) {
            FileLoader fl = new FileLoader(getFile(), model);
        } else if (ae.getActionCommand().equals("Record")) {
            model.setState("RECORD");
            System.out.println("State is now record");
            gui.switchMode();
        } else if (ae.getActionCommand().equals("History")) {
            model.setState("HISTORY");
            System.out.println("State is now history");
            gui.switchMode();
        } else if (ae.getActionCommand().equals("Prescription")) {
            model.setState("PRES");
            System.out.println("State is now prescription");
            gui.switchMode();
        }
        else if (ae.getActionCommand().equals("Notes")) {
            model.setState("NOTES");
            System.out.println("State is now notes");
            gui.switchMode();
        }

        else if (ae.getActionCommand().equals("PatientUp")) {
            System.out.println("Patient Changed Up");
//            model.addObserver(gui);
            model.changePatientUp();

        }
        else if (ae.getActionCommand().equals("PatientDown")) {
            System.out.println("Patient Changed Down");
//            model.addObserver(gui);
            model.changePatientDown();
        }
        else if (ae.getActionCommand().equals("AdmissionUp")) {
            System.out.println("Admission Changed Up");
//            model.addObserver(gui);
            model.changeAdmission();
        }
        else if (ae.getActionCommand().equals("AdmissionDown")) {
            System.out.println("Admission Changed Down");
//            model.addObserver(gui);
            model.changeAdmission();
        }



    }

    private void setFile(File f) {
        file = f;
    }

    private File getFile() {
        return file;
    }
}
