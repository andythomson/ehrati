package controller;

import model.Model;
import view.GUI;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
/**
 * Created by andrew-thomson on 18/11/2017.
 */
public class KeyPressListener implements KeyListener {
	
	private Model model;
    private SchedulerListener sl;
    private GUI gui;
	
	public KeyPressListener(Model inputModel, SchedulerListener inputSchedulerListener, GUI g) {
		model = inputModel;
		sl = inputSchedulerListener;
        gui = g;

	}

	@Override
	public void keyPressed(KeyEvent ke) {

    }
	@Override
	public void keyReleased(KeyEvent ke) {

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}