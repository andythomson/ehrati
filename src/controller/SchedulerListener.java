package controller;

import model.Model;
import view.GUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Created by andrew-thomson on 18/11/2017.
 */
public class SchedulerListener implements ActionListener{
    private Timer runTimer;
    private GUI gui;

    public SchedulerListener(Model m, GUI g){
        gui = g;
        runTimer = new Timer(50, this);
    }

    @Override
    public void actionPerformed(final ActionEvent ae) {
        gui.repaint();
    }

    public void startTimer(){
        runTimer.start();
    }

    public void stopTimer(){
        runTimer.stop();
    }

    public boolean isRunning() {
        return runTimer.isRunning();
    }


}
